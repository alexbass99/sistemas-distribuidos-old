# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

_Todo esto forma parte de una práctica para la asignatura de Sistemas Distribuidos del Grado de Ingeniería Multimedia_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Despliegue** para conocer cómo desplegar el proyecto mediante el *Gestor de repositorios **_Git_***.


### Pre-requisitos 📋

```
· Última version estable de Node JS.
· Gestor de repositorios Git (para poder clonar el repositorio).
· Biblioteca express para Node JS (para aplicar una capa de sobre Node y facilitar el uso de los recursos o métodos HTTP).
· Postman (para poder enviarle peticiones a la API REST).
· Morgan (motor de registro donde se registra cada instrucción entre los clientes y el servicio web).
· MongoDB (base de datos del servicio web)

---------- OPCIONAL ----------
· Nodemon (opcional pero útil ya que permite que actualicemos y refresquemos el proyecto sin necesidad de  reiniciar el proceso).
· JSON Formatter en el navegador (opcional pero necesario para obtener la mejor experiencia de uso en el navegador, en caso de ya tenerlo integrado no es necesario).

```

### Instalación 🔧
- _INSTALACIÓN DE NODE JS_

_A ser posible usaremos la version 16.13.0 que es la que al momento de desarrollar el programa era la última versión estable. Para ello instalaremos el gestor de paquetes de Node, el npm. Gracias a este gestor obtenemos acceso a la herramiente 'n', la cual nos ofrece ayuda durante la instalacion de Node y el mantenimiento de sus versiones._

```
/* Abrimos una terminal mediante las aplicaciones o "Ctrl+Alt+T" */

$ sudo apt update             // Actualizamos el gestor de paquetes
$ sudo apt install npm        // Instalamos el gestor npm
$ sudo npm i -g n             // A través de él obtenemos la herramienta 'n'
$ sudo n stable               // E instalamos a traves de 'n' la última versión estable de Node
```
_Tras haber realizado todos estos procesos en la terminal podemos comprobar la versión de node y de su gestor de paquetes con los siguientes comandos en la terminal_

```
$ node --version              // Versión de Node 
V16.13.0
$ npm -v                      // Versión del gestor de paquetes de Node
8.1.0
```

- _INSTALACIÓN DE EXPRESS_

_Para instalar la librería Express usaremos el gestor de paquetería de Node JS y mediante una simple sentencia lo tendremos ya implementado en nuestro ordenador._

```
$ npm i -S express          // Dejamos que descargue y tendríamos ya completada la instalación del mismo
```

- _INSTALACIÓN DE POSTMAN_

_Postman es el programa que usaremos para crear solicitudes a nuestro servidor y comprobar las respuestas del mismo. Al igual que con el resto de programas usaremos la terminal para instalarlo._

```
$ sudo snap install postman    // Dejamos que descargue y tendríamos ya completada la instalación del mismo
```
_Como podemos observar, ya que Postman no es parte de los paquetes de Node, no usamos el npm, en cambio usamos Snap, uno de muchos gestores de paquetería para instalar aplicaciones._

- _INSTALACIÓN DE MORGAN_

_Morgan es una herramienta esencial ya que es de vital importancia en un sistema de Clientes-Servidor conocer en todo momento que ocurre y que cliente habla al servidor o qué le está preguntando. Mediante la terminal instalamos Morgan de la siguiente manera._

```
$ npm i -S morgan           // Usamos el gestor npm para instalar Morgan
```
- _INSTALACIÓN DE MONGODB_

_No hay servicio web sin su base de datos, pues es una manera rápida y eficaz de organizar las cantidades estratosféricas de datos que mueven y registran. Como ha sido usual durante esta instalación, usamos la terminal y lo instalamos con la siguiente secuencia:_

```
$ sudo apt update                   // Nos aseguramos de actualizar el gestor de paquetes
$ sudo apt install -y mongodb       // E instalamos mongodb y su cliente mongo
```

- _INSTALACIÓN DE NODEMON (opcional)_

_Mencionado ya anteriormente esta es una herramienta opcional, ya que en el caso de querer desarrollar este proyecto presenta una gran utilidad y a la larga ahorra bastante tiempo teniendo en cuenta las veces que tendría que reiniciarse el programa durante los cambios sin su presencia en los archivos del mismo. Necesitaremos ir con la terminal hasta la carpeta donde tenemos el repositorio y ya dentro ejecutamos lo siguiente:_

```
$ npm i -D nodemon      // Como podemos observar ahora estamos usando el npm ya que tiene que ver con Node JS
```
_Tras esto ya lo tendremos instalado y sin necesidad de configurar nada, pues todo esta ya en el código del programa._

- _INSTALACIÓN DE JSON FORMATTER (opcional)_

_Muchos navegadores como Mozilla Firefox ya tienen esta herramienta implementada, en el caso de Google Chrome podemos optar por extensiones que ofrece en su listado de herramientas, un ejemplo de esto sería la siguiente extension: https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa?hl=es ._


## Despliegue 📦

_Lo primero de todo necesitaremos tener el gestor de repositorios **_Git_** instalado. Para ello abrimos como anteriormente se ha mencionado y ejecutamos las siguientes sentencias:_

```
$ sudo apt install git

//Añadimos el argumento --global para poder usar estos datos en todos los proyectos, si deseamos hacerlo individualmente entre cada proyecto solamente necesitamos ir hacia la carpeta de ese proyecto en concreto y ejecutar estos comandos sin el --global

$ git config --global user.name tuNombreDeUsuario   
$ git config --global user.email tuEmail 
$ git config --list
user.name = tuNombreDeUsuario
user.email = tuEmail
```
_Cuando localicemos la carpeta donde queremos tener el repositorio desde la terminal, debemos clonar el repositorio del servidor a nuestro repositorio local, para ello necesitaremos el link del repositorio a clonar y por otro lado pondremos el nombre de la carpeta que se creará y donde se copiarán los datos._

```
$ git clone LinkDelRepositorio NombreDeLaCarpeta 
$ cd NombreDeLaCarpeta
$ git remote -v
$ git remote add origin LinkDelRepositorio      //Determinamos este repositorio como origen 
$ git status                //Para asegurarnos de que la conexión está creada correctamente
$ git fetch origin          //Solicitamos traer los datos del repositorio remoto
$ git pull origin master    //Combinamos esos datos que hemos traido con nuestro repositorio local
```

_Tras haber terminado con todo esto habremos copiado correctamente todos los datos del repositorio remoto hacia nuestro repositorio local, por lo que si cumplimos con los pre-requisitos no tendremos problemas para ejecutar directamente el programa._

## Ejecutando las pruebas ⚙️

_Podemos probar a ejecutar el programa yendo con la terminal a la carpeta del repositorio. Dependiendo de si tenemos instalado o no Nodemon ejecutamos lo siguiente:_

```
$ npm start         // En caso de tener Nodemon instalado
$ node index.js     // En caso de no tenerlo instalado
```
_En ambos casos recibiremos por parte del servidor el siguiente mensaje:_
```
API REST ejecutándose en http://localhost:3000/api/:coleccion/:id
```
_A continuación encenderemos la base de datos mediante el cliente Mongo en otra terminal_
```
$ sudo systemctl start mongodb // Se pueden usar otras ordenes como stop, restart, etc...
$ mongo --host 127.0.0.1:27017  // Accedemos a la bbdd en localhost
```
_Esta terminal se recomienda dejarla a parte ya que nos permite el acceso directo a gestionar la base de datos y su información, y por otro lado tener la del programa con el servidor activo._

_En este momento ya podríamos abrir Postman y comenzar a lanzar peticiones al servidor y a la base de datos, teniendo las dos terminales para comprobar que esta ocurriendo internamente con dichas peticiones, junto a la interfaz de Postman para ver cuales son las respuestas del servidor._


## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [NodeJS](https://nodejs.org) - Herramienta para el back-end del proyecto junto a todas sus extensiones y plugins.
* [MongoDB](https://www.mongodb.com/es) - Base de datos.

## Versionado 📌

Para todas las versiones disponibles, mira los [tags en este repositorio](https://bitbucket.org/alexbass99/sistemas-distribuidos/commits) en el listado de commits del proyecto.

## Autores ✒️

* **Alejandro Martínez Cedillo** - [alexbass99](https://bitbucket.org/alexbass99)

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a todo el mundo que me lo pida. 
* Da las gracias públicamente 🤓.
